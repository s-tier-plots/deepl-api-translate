import os
import json
import requests
import webbrowser
import PySimpleGUI as sg
from decouple import config

from src.util import *


def save_auth_key(api_key):
    with open(".env", "a") as env_file:
        env_file.write(f"DEEPL_KEY={api_key}\n")


def get_auth_key():
    layout = [
        [sg.Text("Please paste your DeepL API key below:")],
        [sg.Multiline(size=(40, 4), key="auth_key_input")],
        [sg.Text("Sign up:"), sg.Text(
            "https://www.deepl.com/pro-api", text_color='blue', enable_events=True, key="deepl_link")],
        [sg.Button("Save"), sg.Button("Cancel")]
    ]
    window = sg.Window(title="Enter DeepL API Key", layout=layout)
    while True:
        event, values = window.read()
        if event == "Save":
            api_key = values["auth_key_input"].strip()
            save_auth_key(api_key)
            window.close()
            return api_key
        elif event == "deepl_link":
            webbrowser.open_new_tab("https://www.deepl.com/pro-api")
        elif event == sg.WIN_CLOSED or event == "Cancel":
            window.close()
            return None


def translate_text_batch(texts, target_lang):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': f"DeepL-Auth-Key {auth_key}",
    }

    translations = []
    for text in texts:
        translated = requests.post(
            url="https://api-free.deepl.com/v2/translate",
            headers=headers,
            data={
                "target_lang": target_lang,
                "text": text,
            },
        )
        try:
            response_data = json.loads(translated.text)
            translations.append(response_data["translations"][0]["text"])
        except:
            return None

    return translations


def translate_file(srt, target_lang):
    print(f'Working on {os.path.basename(srt)}')

    minus_timeline = remove_timeline(srt)
    with open(minus_timeline, 'r', encoding='utf-8') as f:
        all_text = f.read()

    filename = os.path.splitext(srt)[0]
    metadata = f".dl.{target_lang.split('-')[0]}"

    batch_size = 1000
    text_batches = split_text_into_batches(all_text, batch_size)
    translated_batches = translate_text_batch(text_batches, target_lang)
    if translated_batches is None:
        return False

    translated_text = filename + metadata + '.txt'
    with open(translated_text, 'w', encoding='utf-8') as g:
        g.write('\n'.join(translated_batches))

    finalsrt = filename + metadata + '.srt'
    combine_srt(srt, translated_text, finalsrt)
    os.remove(translated_text)
    os.remove(minus_timeline)

    return True


def interactive():
    global auth_key
    auth_key = config('DEEPL_KEY')

    if not auth_key:
        auth_key = get_auth_key()
        if not auth_key:
            print("No API key provided. Exiting.")
            return

    layout = [
        [sg.Text("Select SRT File"), sg.In(size=(20, 1),
                                           enable_events=True, key="srt"), sg.FileBrowse()],
        [sg.Text("Select Target Language"), sg.Combo(['en-US', 'en-GB', 'es-ES',
                                                      'fr-FR', 'ja', 'zh-CN'], size=10, enable_events=True, key='target_lang')],
        [sg.Text("—"*25)],
        [sg.Text(" ", key="status")],
        [sg.Button('Translate'), sg.Button('Exit')]
    ]
    window = sg.Window(title="Deepl Translate GUI", layout=layout)
    while True:
        event, values = window.read(timeout=0.5)
        filename = os.path.basename(values["srt"])
        if event == "OK" or event == sg.WIN_CLOSED:
            break
        if event == "-File-":
            pass
        if event == "Translate":
            result = translate_file(values["srt"], values["target_lang"])
            if result == False:
                window['status'].update(f"Status: Error ({filename})")
            else:
                window['status'].update(f"Status: Complete ({filename})")

        if event in (None, 'Exit'):
            return exit
            break


interactive()
