import re
import os


def remove_timeline(txtpath):
    with open(txtpath, 'r', encoding='utf-8') as f:
        contents = f.read()
        # remove num + timeline
        sub1 = re.sub(
            r'\d+\n\d\d:\d\d:\d\d,\d\d\d --> \d\d:\d\d:\d\d,\d\d\d', '', contents, 0, re.M)
        sub1 = re.sub(r'\n', '', sub1, 1, re.M)
        sub1 = re.sub(r'\n\n', '\n', sub1, 0, re.M)

    os.path.splitext(txtpath)[0]
    with open(os.path.splitext(txtpath)[0] + '.og.txt', 'w', encoding='utf-8') as f:
        f.write(sub1)

    return os.path.splitext(txtpath)[0] + '.og.txt'


def combine_srt(mysrt, wordtxt, finalsrt):
    with open(mysrt, 'r', encoding='utf-8') as f:
        srt = f.read()
        match = re.findall(r'\d+:\d+:\d+,\d+ --> \d+:\d+:\d+,\d+', srt)

    linerList = []
    liner = ""
    with open(wordtxt, "r", encoding="utf-8", errors='ignore') as wordfile:
        lines = wordfile.readlines()
        for line in lines:
            if line != '\n' and line is not lines[-1]:
                liner += line
            elif line != '\n' and len(linerList) == len(match)-1:
                liner += line
                linerList.append(liner)
                break
            else:
                linerList.append(liner)
                liner = ""

    count = 0
    with open(finalsrt, 'w', encoding='utf-8') as resfile:
        for timeline in match:
            resfile.write(f"{count+1}\n")
            resfile.write(timeline+'\n')
            resfile.write(linerList[count])
            resfile.write("\n")
            count += 1


def split_text_into_batches(text, batch_size):
    lines = text.split('\n')
    batches = [lines[i:i + batch_size]
               for i in range(0, len(lines), batch_size)]
    return ['\n'.join(batch) for batch in batches]
