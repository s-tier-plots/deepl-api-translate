# Deepl API Translator

Simply run the compiled `.exe` or continue below for running through terminal.

### Development

Deepl Free API auth key required. [Deepl API](https://www.deepl.com/pro-api)

Create `.env` file in root folder and paste auth key as such:

```
DEEPL_KEY=*****************************
```

### Source Install

```
cd source
pip install -r requirements.txt
```

_Tested and created with Python 3.8_
